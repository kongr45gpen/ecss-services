#include "Services/HousekeepingService.hpp"
#include "Message.hpp"
#include "ServicePool.hpp"

void HousekeepingService::checkAndSendHousekeepingReports(uint32_t timestamp) {
	for (auto it = housekeepingStructureList.begin(); it != housekeepingStructureList.end(); ++it) {
		uint32_t diffTime = timestamp - it->second.timestamp;
		if (it->second.isPeriodic && (diffTime >= it->second.collectionInterval)) {

			it->second.timestamp = timestamp;

			Message report = createTM(25); // create TM[3.25]
			report.appendByte(it->first); // append housekeeping structure ID

			for (auto i : it->second.paramId) {
				if (i < systemParameters.parametersArray.size()) {
					systemParameters.parametersArray.at(i).get().appendValueToMessage(report);
				} else {
					ErrorHandler::reportError(report, ErrorHandler::ExecutionStartErrorType::UnknownExecutionStartError);
				}
			}
			storeMessage(report);
		}
	}
}

void HousekeepingService::enablePeriodicParamReports(Message& message) {
    // Check if the packet is correct
    message.assertTC(3, 5);

    uint8_t idCount = message.readUint8();
    for (uint8_t i = 0; i < idCount; i++) {
        auto it = housekeepingStructureList.find(message.readUint8());
        if (it != housekeepingStructureList.end()) {
            it->second.isPeriodic = true;
        } else {
            ErrorHandler::reportError(message, ErrorHandler::ExecutionStartErrorType::EventActionUnknownEventDefinitionError);
        }
    }
}

void HousekeepingService::disablePeriodicParamReports(Message& message) {
    message.assertTC(3, 6);

    uint8_t idCount = message.readUint8();
    for (uint8_t i = 0; i < idCount; ++i) {
        auto it = housekeepingStructureList.find(message.readUint8());
        if (it != housekeepingStructureList.end()) {
            it->second.isPeriodic = false;
        } else {
            ErrorHandler::reportError(message, ErrorHandler::ExecutionStartErrorType::EventActionUnknownEventDefinitionError);
        }
    }
}

void HousekeepingService::modifyCollectionInterval(Message& message) {
    message.assertTC(3, 31);

    uint8_t idCount = message.readUint8();
    for (uint8_t i = 0; i < idCount; ++i) {
        auto it = housekeepingStructureList.find(message.readUint8());
        if (it != housekeepingStructureList.end()) {
            it->second.collectionInterval = message.readUint16();
        } else {
            ErrorHandler::reportError(message, ErrorHandler::ExecutionStartErrorType::EventActionUnknownEventDefinitionError);
        }
    }
}

void HousekeepingService::execute(Message& message) {
    switch (message.messageType) {
        case 5: enablePeriodicParamReports(message);
            break;
        case 6: disablePeriodicParamReports(message);
            break;
        case 31: modifyCollectionInterval(message);
            break;
        default: ErrorHandler::reportError(message, ErrorHandler::OtherMessageType);
    }
}

