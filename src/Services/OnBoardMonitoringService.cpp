#include "Services/OnBoardMonitoringService.hpp"
#include "Services/OnBoardMonitoringChecks.hpp"

void OnBoardMonitoringService::addParameterMonitoringDefinition(Message &message) {
    message.assertTC(12, 5);
    auto N = message.readUint8();

    for (int i = 0; i < N; i++) {
        auto pmonId = message.readUint16();
        auto parameterId = message.readUint16();
        auto validityID = message.readUint16();
        auto mask = message.readUint64();
        auto expectedValue = message.readUint64();
        auto interval = message.readUint16();
        auto repetitions = message.readUint8();
        auto checkType = static_cast<CheckTypeID>(message.readUint8());

        bool validityCheck = mask != 0;
        CheckValidityCondition condition { validityID, mask, expectedValue };
        std::optional<CheckValidityCondition> optionalCondition = validityCheck ? std::optional(condition) : std::nullopt;

        if (checkType == CheckTypeID::ExpectedValue) {
            ExpectedValueCheck<uint64_t> check {
                message.readUint64(),
                message.readUint64(),
                message.readUint16()
            };

            using Definition = ParameterMonitoringDefinition<float, ExpectedValueCheck<uint64_t>>;

            Definition* definition = new (monitoringDefinitionPool.allocate<Definition>())
                    Definition( pmonId, parameterId, interval, repetitions, check, optionalCondition);

            addParameterMonitoringDefinition(*definition);
        } else if (checkType == CheckTypeID::Limit) {
            LimitCheck<float> check {
                    message.readFloat(),
                    message.readUint16(),
                    message.readFloat(),
                    message.readUint16()
            };

            using Definition = ParameterMonitoringDefinition<float, LimitCheck<float>>;

            Definition* definition = new (monitoringDefinitionPool.allocate<Definition>())
                    Definition( pmonId, parameterId, interval, repetitions, check, optionalCondition);

            addParameterMonitoringDefinition(*definition);
        }
    }
}

void OnBoardMonitoringService::deleteParameterMonitoringDefinition(Message &message) {
    message.assertTC(12, 6);
    auto N = message.readUint8();

    for (int i = 0; i < N; i++) {
        auto pmonId = message.readUint16();

        auto it = std::find_if(parameterMonitoringDefinitions.begin(), parameterMonitoringDefinitions.end(), [pmonId](std::reference_wrapper<ParameterMonitoringDefinitionBase> pmon) {
            return pmon.get().getPmonId() == pmonId;
        });

        if (it == parameterMonitoringDefinitions.end()) {
            ErrorHandler::reportError(message, ErrorHandler::ParameterMonitoringDefinitionUnknown);
        } else {
            parameterMonitoringDefinitions.erase(it);
        }
    }
}

void OnBoardMonitoringService::enableParameterMonitoringDefinitions(Message& message) {
    message.assertTC(12, 1);

    auto N = message.read<uint8_t>();

    for (int i = 0; i < N; i++) {
        auto pmonId = message.read<uint16_t>();

        bool found = false;
        for (auto& it: parameterMonitoringDefinitions) {
            if (it.get().getPmonId() == pmonId) {
                it.get().setEnabled(true);
                found = true;
            }
        }

        if (not found) {
            ErrorHandler::reportError(message, ErrorHandler::ExecutionStartErrorType::UnknownExecutionStartError);
        }
    }
}

void OnBoardMonitoringService::disableParameterMonitoringDefinitions(Message& message) {
    message.assertTC(12, 2);

    auto N = message.read<uint8_t>();

    for (int i = 0; i < N; i++) {
        auto pmonId = message.read<uint16_t>();

        bool found = false;
        for (auto& it: parameterMonitoringDefinitions) {
            if (it.get().getPmonId() == pmonId) {
                it.get().setEnabled(false);
                found = true;
            }
        }

        if (not found) {
            ErrorHandler::reportError(message, ErrorHandler::ExecutionStartErrorType::UnknownExecutionStartError);
        }
    }
}

void OnBoardMonitoringService::enableParameterMonitoringFunction(Message& message) {
    message.assertTC(12, 15);
    monitoringEnabled = true;
}

void OnBoardMonitoringService::disableParameterMonitoringFunction(Message& message) {
    message.assertTC(12, 16);
    monitoringEnabled = false;
}

void OnBoardMonitoringService::reportParameterMonitoringDefinitions(Message &message) {
    message.assertTC(12, 8);

    Message genericResponse(12, 9, Message::TM, 0);

    auto N = message.readUint8();

    uint8_t count = 0;
    genericResponse.appendUint8(1); // Placeholder for later

    if (N == 0) {
        for (auto &it: parameterMonitoringDefinitions) {
            Message response = genericResponse;
            it.get().appendDefinitionReport(response);
            storeMessage(response);
            count++;
        }
    } else {
        for (int i = 0; i < N; i++) {
            // Find parameter monitoring definition with ID
            auto pmonId = message.readUint16();
            bool found = false;
            for (auto &it: parameterMonitoringDefinitions) {
                if (it.get().getPmonId() == pmonId) {
                    found = true;
                    Message response = genericResponse;
                    it.get().appendDefinitionReport(response);
                    storeMessage(response);
                    count++;
                    break;
                }
            }

            if (!found) {
                ErrorHandler::reportError(message, ErrorHandler::ParameterMonitoringDefinitionUnknown);
            }
        }
    }
};

void OnBoardMonitoringService::checkTransitionReport() {
    if (checkTransitionList.empty()) return;

    Message message = createTM(12);

    message.appendUint8(checkTransitionList.size());

    for (const auto& it : checkTransitionList) {
        message.append(it.pmonId);
        message.append(it.monitoredParameterId);
        message.append(static_cast<uint8_t>(it.checkType));
        message.append(it.parameterValue);
        message.append(static_cast<uint8_t>(it.previousStatus));
        message.append(static_cast<uint8_t>(it.currentStatus));
        message.append(it.transitionTime);
    }

    checkTransitionList.clear();
    storeMessage(message);
}

void OnBoardMonitoringService::execute(Message& message) {
    switch (message.messageType) {
        case 1:
            enableParameterMonitoringDefinitions(message);
            break;
        case 2:
            disableParameterMonitoringDefinitions(message);
            break;
        case 5:
            addParameterMonitoringDefinition(message);
            break;
        case 6:
            deleteParameterMonitoringDefinition(message);
            break;
        case 8:
            reportParameterMonitoringDefinitions(message);
            break;
        case 15:
            enableParameterMonitoringFunction(message);
            break;
        case 16:
            disableParameterMonitoringFunction(message);
            break;
        default:
            ErrorHandler::reportError(message, ErrorHandler::OtherMessageType);
            break;
    }
}
