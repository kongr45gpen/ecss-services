#ifndef ECSS_SERVICES_PARAMETER_HPP
#define ECSS_SERVICES_PARAMETER_HPP

#include "etl/String.hpp"
#include "Message.hpp"
#include "ECSS_Definitions.hpp"
#include <atomic>

/**
 * Implementation of a Parameter field, as specified in ECSS-E-ST-70-41C.
 *
 * @author Grigoris Pavlakis <grigpavl@ece.auth.gr>
 * @author Athanasios Theocharis <athatheoc@gmail.com>
 *
 * @section Introduction
 * The Parameter class implements a way of storing and updating system parameters
 * of arbitrary size and type, while avoiding std::any and dynamic memory allocation.
 * It is split in two distinct parts:
 * 1) an abstract \ref ParameterBase class which provides a
 * common data type used to create any pointers to \ref Parameter objects, as well as
 * virtual functions for accessing the parameter's data part, and
 * 2) a templated \ref Parameter used to store any type-specific parameter information,
 * such as the actual data field where the parameter's value will be stored.
 *
 * @section Architecture Rationale
 * The ST[20] Parameter service is implemented with the need of arbitrary type storage
 * in mind, while avoiding any use of dynamic memory allocation, a requirement for use
 * in embedded systems. Since lack of Dynamic Memory Access precludes usage of stl::any
 * and the need for truly arbitrary (even for template-based objects like etl::string) type storage
 * would exclude from consideration constructs like etl::variant due to limitations on
 * the number of supported distinct types, a custom solution was needed.
 * Furthermore, the \ref ParameterService should provide ID-based access to parameters.
 */
class ParameterBase {
public:
    uint16_t id;

    virtual void appendValueToMessage(Message &message) = 0;

    virtual void setValueFromMessage(Message &message) = 0;

    virtual uint64_t convertValue() = 0;
    virtual float convertFloat() = 0;
};

template<typename DataType>
class ReadableParameter : public ParameterBase {
public:
    using Type = DataType;

    virtual DataType getValue() = 0;

    virtual uint64_t convertValue() override {
        return static_cast<uint64_t>(getValue());
    }

    virtual float convertFloat() override {
        return static_cast<float>(getValue());
    }
};

/**
 * Implementation of a parameter containing its value. See \ref ParameterBase for more information.
 * @tparam DataType The type of the Parameter value. This is the type used for transmission and reception
 * as per the PUS.
 */
template<typename DataType>
class Parameter : public ReadableParameter<DataType> {
private:
    std::atomic<DataType> currentValue;

public:
    explicit Parameter(DataType initialValue) : currentValue(initialValue) {}

    inline void setValue(DataType value) {
        currentValue = value;
    }

    DataType getValue() override {
        return currentValue;
    }

    /**
     * Given an ECSS message that contains this parameter as its first input, this loads the value from that paremeter
     */
    inline void setValueFromMessage(Message &message) override {
        if constexpr (std::is_enum<DataType>::value) {
            using UnderlyingType = typename std::underlying_type<DataType>::type;

            currentValue = static_cast<DataType>(message.read<UnderlyingType>());
        } else {
            currentValue = message.read<DataType>();
        }
    };

    /**
     * Appends the parameter as an ECSS value to an ECSS Message
     */
    inline void appendValueToMessage(Message &message) override {
        if constexpr (std::is_enum<DataType>::value) {
            using UnderlyingType = typename std::underlying_type<DataType>::type;

            message.append<UnderlyingType>(static_cast<UnderlyingType>(currentValue.load()));
        } else {
            message.append<DataType>(currentValue.load());
        }
    };
};

template<typename DataType>
class FunctionParameter : public ReadableParameter<DataType> {
public:
    FunctionParameter(const std::function<DataType()> &calculator) : calculator(calculator) {}

    DataType getValue() {
        return calculator();
    }

    void setValueFromMessage(Message &message) override {
        ErrorHandler::reportError(message, ErrorHandler::WriteToReadOnlyParameter);
    }

    void appendValueToMessage(Message &message) override {
        if constexpr (std::is_enum<DataType>::value) {
            using UnderlyingType = typename std::underlying_type<DataType>::type;

            message.append<UnderlyingType>(static_cast<UnderlyingType>(getValue()));
        } else {
            message.append<DataType>(getValue());
        }
    };
private:
    std::function<DataType()> calculator;
};

template<class DataType>
class CallbackParameter : public ReadableParameter<DataType> {
public:
    CallbackParameter(DataType initialValue, const std::function<void(DataType&)> &callback) : currentValue(initialValue), callback(callback) {}

    inline void setValue(DataType value) {
        if (value != currentValue) {
            callback(value);
            currentValue = value;
        }
    }

    DataType getValue() override {
        return currentValue;
    }

    /**
     * Given an ECSS message that contains this parameter as its first input, this loads the value from that paremeter
     */
    inline void setValueFromMessage(Message &message) override {
        if constexpr (std::is_enum<DataType>::value) {
            using UnderlyingType = typename std::underlying_type<DataType>::type;

            setValue(static_cast<DataType>(message.read<UnderlyingType>()));
        } else {
            setValue(message.read<DataType>());
        }
    };

    /**
     * Appends the parameter as an ECSS value to an ECSS Message
     */
    inline void appendValueToMessage(Message &message) override {
        if constexpr (std::is_enum<DataType>::value) {
            using UnderlyingType = typename std::underlying_type<DataType>::type;

            message.append<UnderlyingType>(static_cast<UnderlyingType>(currentValue.load()));
        } else {
            message.append<DataType>(currentValue.load());
        }
    };
private:
    std::atomic<DataType> currentValue;
    std::function<void(DataType&)> callback;
};

#endif //ECSS_SERVICES_PARAMETER_HPP
